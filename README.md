The align-it-ob3 tool is designed for molecular alignment using OpenBabel3, a chemical toolbox designed to speak the many languages of chemical data. This tool is particularly useful for aligning molecular structures based on their shapes and chemical properties, which is a common task in cheminformatics and computational chemistry.

To use this tool, you need to install Docker and then follow the instructions to build and install the align-it software. The general steps include cloning the repository, creating a build directory, configuring the build with CMake, and then compiling and installing the software.

Here’s a brief outline of the steps involved in using align-it-ob3:

Install OpenBabel3: This is a prerequisite for using align-it-ob3.
Clone the Repository: Get the source code from the GitHub repository.
Build and Install: Use CMake and make to build the software from the source code.


# OpenBabel Project Docker Setup

## Install Docker

Follow the tutorial [here](https://docs.rockylinux.org/gemstones/containers/docker/) to install Docker on Rocky Linux.

## Clone the Repository

```sh
git clone https://github.com/krishiv-g/align-it-ob3.git
cd align-it-ob3
```

## Download openbable
```sh
wget https://github.com/openbabel/openbabel/archive/refs/tags/openbabel-3-1-1.tar.gz
tar zxf openbabel-3-1-1.tar.gz
```

## Build the Docker Image

```sh
docker build -t openbabel-project .
```
## CREATE SHARED FOLDER 
```sh
mkdir -p /home/align
```


## Run the Docker Container

```sh
docker run -d --name openbabel-container -v /home/align:/home openbabel-project

```

## TO GO INSIDE CONTAINER 

```sh
docker exec -it openbabel-container /bin/bash
```



## TO SHARE FILES WITH CONTAINER 

### Put files on /home/align  in the server , Anything that is inside /home/align will be as is avilable on /home inside container 